load_module /usr/lib/nginx/modules/ngx_http_js_module.so;
worker_processes  8;
error_log /dev/stdout error;

events {
    worker_connections  1024;
}

http {
    default_type        application/octet-stream;
    sendfile            on;
    keepalive_timeout   65;

    # image-resolver-cache definition
    proxy_cache_path /var/cache/nginx/image-resolver
                     levels=1:2 keys_zone=image-resolver-cache:8m
                     max_size=1g inactive=1h use_temp_path=off;

    access_log /dev/stdout;
    js_import proxy from proxy.js;
    js_set $destURL proxy.destURL;
    js_set $imageId proxy.imageId;
    js_set $token proxy.token;

    server {
        listen 80;

        location / {
            root /welcome-content;
        }

        location /admin {
            include  /etc/nginx/mime.types; 
            alias /www/eaas-frontend-admin/dist;
        }

        location /landing-page {
            include  /etc/nginx/mime.types; 
            alias /www/landing-page/dist;
        }
    }

    server {
        listen          81;

        root /image-archive/;
        resolver 127.0.0.11 ipv6=off;
        location @forward {
            proxy_set_header Authorization $token;
            proxy_pass $destURL;	
        }
        
        location / {
            try_files \
                /emulators/images/fakeqcow/$imageId \
		/emulators/images/base/$imageId \
		/meta-data/$imageId \
		/public/images/base/$imageId \
		/public/images/derivate/$imageId \
		/public/images/user/$imageId \
                /public/images/containers/$imageId \
		/public/images/checkpoints/$imageId \
		/public/images/object/$imageId \
		/images/base/$imageId \
		/images/derivate/$imageId \
		/images/user/$imageId \
                /images/containers/$imageId \
		/images/checkpoints/$imageId \
		/images/object/$imageId \
                /images/sessions/$imageId \
		/images/tmp/$imageId \
		/images/roms/$imageId \
        /images/runtime/$imageId \
                @forward;  
        }
    }

    server {
        listen          82;
        resolver 127.0.0.11 ipv6=off;

        location / {
            root /image-archive/;
            try_files \
                /emulators/images/base/$imageId \
                /meta-data/$imageId \
                @images;
        }

        location @images {
            {%- if eaas_gw_hostname == 'localhost' %}
                {% set baseurl = 'http://eaas:8080' %}
            {%- else %}
                {%- set baseurl = eaas_protocol + eaas_gw_hostname + eaas_base_url_port %}
            {%- endif %}

            proxy_pass {{ baseurl }}/emil/components/resolve$uri;
            add_header X-Cache-Status $upstream_cache_status;

            # custom cache config
            proxy_cache image-resolver-cache;
            proxy_cache_key $request_method+$scheme+$proxy_host$request_uri;
            proxy_cache_valid 301 307 30m;
            proxy_cache_convert_head off;
            proxy_cache_lock on;  # only one concurrent update per key!
        }
    }
}
